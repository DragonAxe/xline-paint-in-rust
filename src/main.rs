extern crate ncurses;

use ncurses::*;

fn main()
{
    // Start ncurses.
    initscr();

    // Init ncurses.
    cbreak(); // use raw() to disable ^C.
    noecho();
    keypad(stdscr(), true);
    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);

    // Print welcome.
    printw("Welcome to XLine Paint, a totally (not) official MS paint clone!\n\n");
    printw("Q to quit\n");
    printw("Up, Right, Left, Down to paint.\n\n");
    printw("Press any key to continue.\n");

    // Clean up welcome
    getch();
    clear();

    let mut x:i32 = 5;
    let mut y:i32 = 5;

    // Arrow key draw loop.
    loop {
        // 'X' current cursor position.
        mvaddch(y, x, 'X' as u32);
        refresh();
        
        // Wait for input, then move cursor leaving a trail of 'x'.
        let c = getch();
        mvaddch(y, x, 'x' as u32);
        
        match c {
            KEY_DOWN => y = y + 1,
            KEY_UP => y = y - 1,
            KEY_LEFT => x = x - 1,
            KEY_RIGHT => x = x + 1,
            c if c == ('q' as i32) => break,
            _ => {}
        }
    }
    
    // Terminate ncurses.
    endwin();
}
